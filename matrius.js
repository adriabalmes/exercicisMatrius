let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}
function paintAll() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) { // filas
        for (let j = 0; j < matrix[i].length; j++) { // columnas
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (let i = 0; i < matrix.length; i++) { // filas
        for (let j = 0; j < matrix[i].length; j++) { // columnas
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

function paintRightHalf() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // filas
        for (let j = Math.round(matrix.length/2); j < matrix[i].length; j++) { // columnas
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintLeftHalf() {
    erase();
    for (let i = 0; i < matrix.length; i++) { // filas
        for (let j = 0; j < matrix[i].length/2; j++) { // columnas
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintUpperHalf() {
    erase();

    for (let i = 0; i < matrix.length/2; i++ ) { // filas
        for (let j = 0; j < matrix[i].length; j++) { // columnas
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();

    for (let i = 0; i < matrix.length; i++ ) { // filas
        for (let j = 0; j < i  ; j++) { // columnas
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintUpperTriangle() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) { // filas
        for (let j = i; j < matrix[i].length ; j++) { // columnas
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintPerimeter() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) { // filas
        if (i==0 || i == matrix.length-1) {
            for (let j = 0; j < matrix[i].length; j++) {
                matrix[i][j].style.backgroundColor = "red";
            }
        }
        else {
            for (let j = 0; j < matrix[i].length; j+=4) { // columnas
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
 }

function paintCheckerboard() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) { // filas
        if (i%2==0) {
            for (let j = 0; j < matrix[i].length; j+=2) { // columnas
                matrix[i][j].style.backgroundColor = "red";
            }
        }
        else {
            for (let j = 1; j < matrix[i].length; j+=2) { // columnas
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintCheckerboard2() {
    erase();
    for (let i = 0; i < matrix.length; i++ ) { // filas
        if (i%2==0) {
            for (let j = 1; j < matrix[i].length; j+=2) { // columnas
                matrix[i][j].style.backgroundColor = "red";
            }
        }
        else {
            for (let j = 0; j < matrix[i].length; j+=2) { // columnas
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintNeighbours() {
    let inputX = document.getElementById("inputX").valueAsNumber;
    let inputY = document.getElementById("inputY").valueAsNumber;
    erase();
        for (let i = inputY - 1; i <= inputY + 1; i++) { //  Y
            for (let j = inputX - 1; j <= inputX + 1; j++) { //  X
                try {    
                    if (i == inputY && j == inputX + 1) {
                        for (let j = inputX; j <= inputX; j++) { // X
                            matrix[i][j].style.backgroundColor = "white";
                            // el test de prueba da 'incorrecto' ya que no está planteada con white en el test, pero funciona correctamente
                        }
                    }
                    matrix[i][j].style.backgroundColor = "red";
                }
                catch {
                }
            }
    } 
}

function countNeighbours(x, y) {
    let count = 0;
    for (let i = y - 1; i <= y + 1; i++) { // Y
        for (let j = x - 1; j <= x + 1; j++) { // X
            try {
                if (i==y && j==x) {
                }
                else if (matrix[i][j].style.backgroundColor == "red") {
                    count++;
                }
            }
            catch {
            }
        }
    }
    return count;
}

function paintAllNeighbours() {
    let count;
    for (let i = 0; i < matrix.length; i++ ) { 
        for (let j = 0; j < matrix[i].length; j++) { 
            count = countNeighbours(j, i);
            matrix[i][j].innerText = count;
        }
    }
} 